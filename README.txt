#######################
####--README FILE--####
#######################

***************************
Tiberius Project (2014/15)
***************************

Current members and responsibilities:
=====================================

Dr Jim Herd - Project Manager/Head of the project

Sean Cunningham - Power System

Tasos Deligiannis - Image Processing

Dionysis Koutavas - Databases/Communication

Denis Melehovs - Autonomy 

=========
Passwords
==========

Tiberius WiFi
-------------
Password: hailtiberius
username: admin
password: 1234

Database Unit:
--------------
Password: tiberiusmemory
IP address: 192.168.2.100

Sensors Unit:
-------------
Password: tiberiussenses
IP address: 192.168.2.101

Control Unit:
-------------
Password: tiberiusbrain
IP address: 192.168.2.102

Object Detection Unit:
----------------------
Password: tiberiuseyes
IP address: 192.168.2.103


